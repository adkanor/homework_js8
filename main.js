"use strict";
// THEORY 1.........
// 1. DOM -  это объектная модель документа, которую браузер создает в памяти компьютера на основании HTML-кода, полученного им от сервера
// 2.innerText покажет весь текст из документа html, контент вытащит из тэгов,  а innerHTML покажет текстовную инфу по одному елементу  с тегами, которые окружают этот текст.
// 3.Очень много способов (найти по айди, найти по имени, найти по классу, или найти по css селектору).Я бы обращалась по id,  так как он уникальный,либо querySelector
// EXERSICE 1.........

let allParagraphs = document.getElementsByTagName("p");
console.log(allParagraphs);
document.body.style.backgroundColor = "#ff0000";

// EXERSICE 2..............

let optionsList = document.getElementById("optionsList");
console.log(optionsList);
let optionsListFather = optionsList.parentElement;
console.log(optionsListFather);
if (optionsList.hasChildNodes()) {
  let optionsListChildren = optionsList.childNodes;
  for (let child of optionsListChildren) {
    console.log(child);
    if (child.nodeType == 1) {
      console.log("Element Node");
    } else if (child.nodeType == 3) {
      console.log("Text Node");
    } else {
      console.log(child.nodeType);
    }
  }
  console.log(optionsListChildren);
}

// EXERSICE 3............

let newParagraph = document.getElementById("testParagraph");
console.log(newParagraph);
newParagraph.textContent = " This is a paragraph";

// EXERSICE 4-5............

let allElements = document.body.children;
console.log(allElements);

let headerElements = document.body.children[0].children;
console.log(headerElements);
for (let child of headerElements) {
  child.classList.add("nav-item");
}
console.log(headerElements);

// EXERSICE 6............

let someElements = document.querySelectorAll(".section-title");
console.log(someElements);
for (let elem of someElements) {
  elem.classList.remove("section-title");
}
console.log(someElements);
